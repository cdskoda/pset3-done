
		P R E L I M I N A R Y    S P E C I F I C A T I O N

					 Due 2:00 AM, Friday, 13 October 2017

CPSC 323   Homework #3   A bash-like Macroprocessor

REMINDERS:  Do not under any circumstances copy another student's code or give
a copy of your code to another student.  After discussing the assignment with
another student, do not take any written or electronic record away and engage
in a full hour of mind-numbing activity before you work on it again.  Such
discussions should be noted in your log file.

Sharing with another student ANY written or electronic document (e.g., code or
test cases) related to the course is a violation of the collaboration policy.

Since code reuse is an important part of programming, you may incorporate
published code (e.g., from textbooks or the Net) in your programs, provided
that you give proper attribution in your source and in your statement of major
difficulties AND THAT THE BULK OF THE CODE SUBMITTED IS YOUR OWN.


(30 points) /bin/bash (the Bourne Again SHell) performs variable expansion on
the command line.  For example, if the value of the environment variable HWK is
the string "/c/cs323/Hwk3", then the command

  % $HWK/test.Expand

is expanded to

  % /c/cs323/Hwk3/test.Expand

More generally, bash performs a large set of variable expansions, including

  Syntax        Action
  ~~~~~~        ~~~~~~
  $NAME         Replaced by the value of the environment variable NAME, or by
		the empty string if NAME is not defined.

  ${NAME}       Replaced by the value of the environment variable NAME, or by
		the empty string if NAME is not defined.

  ${NAME-WORD}  Replaced by the value of NAME, or by the expansion of WORD if
		NAME is not defined.

  ${NAME=WORD}  Replaced by the value of the environment variable NAME, or
		by the expansion of WORD if NAME is not defined (in which
		case NAME is immediately assigned the expansion of WORD).

  $0, ..., $9   Replace $D (where D is a decimal digit) by the D-th command
		line argument to Expand, or by the empty string if there is no
		D-th argument.

  ${N}          Replaced by the N-th argument to Expand, or by the empty string
		if there is no N-th argument.

  $*            Replaced by a list of all arguments to Expand (not including
		$0), separated by single space characters.

where

  * NAME is a maximal sequence of one or more alphanumeric or _ characters that
    does not begin with a digit;

  * WORD is any sequence of characters that ends with the first } not escaped
    by a backslash and not within any of the expansions above; and

  * N is a nonempty sequence of digits.

Note that the expansion of WORD takes place before the substitution so that the
search for substrings to expand proceeds from left to right and continues at
the end of the replacement string after each substitution.

The escape character \ removes any special meaning that is associated with the
following non-null, non-newline character.  This can be used to include $, {, },
\, and whitespace (but not newlines) in a command.  The \ is not removed.

Collectively these expansions turn one stage in the front-end of bash into a
macroprocessor with a somewhat unusual syntax (e.g., when compared with that of
the C preprocessor or M4).

Your task is to implement this stage in Perl or Python or Ruby; i.e., to write
a bash-like macroprocessor "Expand" that

* reads lines from the standard input,

* performs the variable expansions described above, and

* writes the expanded command to the standard output.

Examples:

  % /c/cs323/Hwk3/Expand
  (1)$ OSTYPE = $OSTYPE
  >> OSTYPE = linux
  (2)$ ${OSTYPE-Linux}
  >> linux
  (3)$ ${NONEXISTENT}
  >>
  (4)$ ${NONEXISTENT=VALUE}
  >> VALUE
  (5)$ ${NONEXISTENT}
  >> VALUE
  (6)$ $0
  >> /c/cs323/Hwk3/Expand
  (7)$ \$OSTYPE
  >> \$OSTYPE
  (8)$ ${ANOTHER=$OSTYPE}
  >> linux
  (9)$ $ANOTHER
  >> linux
   
Use the submit command to turn in your source and log files for Expand as
assignment 3.

YOU MUST SUBMIT YOUR FILES (INCLUDING THE LOG FILE) AT THE END OF ANY SESSION
WHERE YOU HAVE SPENT AT LEAST ONE HOUR WRITING OR DEBUGGING CODE, AND AT LEAST
ONCE EVERY HOUR DURING LONGER SESSIONS.  (All submissions are retained.)


Notes
~~~~~
1. The environment is a list of name-value pairs that is passed to an executed
   program in the same way as a normal argument list.  The names must be NAMEs
   and the values can be any null-terminated character strings.  The command
   /usr/bin/env prints all environment variables and their values; the command
   /usr/bin/printenv prints the value of its argument.  See [Matthew and
   Stones, pp. 144-148] for details.

   To access the environment variables you can use the hash %ENV in Perl, the
   mapping object os.environ in Python, and the hash-like accessor ENV in Ruby.

2. A $ begins an expansion ONLY when not escaped and immediately followed by
   * an alphabetic or _ as in $NAME
   * a digit as in $0, ..., $9
   * a { as in ${NAME}, ${NAME-WORD}, ${NAME=WORD}, or ${N}
   * a * as in $*
   Otherwise it is just another character.

3. Expand detects the following errors and prints a one-line message to stderr:
   * ${ not followed by an alphanumeric or _
   * ${NAME not followed by a } or - or =
   * A WORD field that is not terminated by a }
   * ${N not followed by a }
   Like bash, it writes only one message even when there are many errors in a
   single line.

4. If WORD contains macros that have to be expanded, it must recursively
   expand them before performing the substitution (e.g., see Example (8)$
   above).  However, if NAME exists, then during the expansion any assignments
   to environment variables must be suppressed.  This functionality will be
   worth at most 6 points (i.e., WORD will contain macros in at most 6 tests).

5. [Matthew and Stones, pp. 27-31, 70-72] and the man page for bash ("man bash"
   or "info bash") contain more complete descriptions of these expansion rules
   (and others that Expand does not implement) as well as examples of their
   use.

   If you use bash to improve your understanding of how the expansion process
   works, bear in mind the following differences between Expand and bash:

   * bash prompts for additional lines in the situations described in Note 3.

   * bash allows escaped newlines and WORD fields that span multiple lines,
     prompting for additional input lines.

   * bash allows the use of single and double quotes to remove the special
     meaning of the characters within quotes.

   * bash also does brace expansion, tilde expansion, command substitution,
     arithmetic expansion, word splitting, and pathname expansion.

   * bash expands shell variables as well as environment variables.

   * bash defines other variable expansions and special macros such as $@, $#,
     $?, $-, $$, or $!.

   * bash tokenizes the command BEFORE expanding variables, which gives a
     somewhat different effect than tokenizing after expanding macros.

   Note: This list will expand as other discrepancies are noted.

6. Like bash, Expand does not print whitespace-only lines.

7. Although all test scripts redirect stdin to a file, you should think of
   Expand as an interactive program that handles one line at a time and takes
   the appropriate action.  Thus if there are multiple lines and only one
   contains an error, then the expansions of the correct lines should be
   printed (to stdout) as well as the error (to stderr).
    
8. Expand writes a newline on reaching the end of the input.

9. Hwk3/Expand runs a Perl solution that contains 40 lines of code (ignoring
   comments and blank/brace-only lines).  A Python solution contains 53 lines
   (ignoring comments and blank/brace-only/continue-only lines).  A C version
   would be _much_ longer.
								CS-323-09/29/17
								